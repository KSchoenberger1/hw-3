Write another brief report that summarizes the performance results you measured. Create plots
showing the strong scaling speed-up and efficiency and the weak scaling efficiency just as you did
for HW2. 

Also, plot the communication run-time (which should be less than the total run-time perstep.)

How does the MPI scalability compare to the OpenMP study? 

Does the code scale better or worse?

What is the ratio of the communication time to the total time and how does that change with the #
of processes in the strong and weak scaling studies?

Also, include a summary of code modifications that were necessary. Discuss difficulties you
encountered and what you did you overcome them. You report should be self-contained in a single
document. Incorporate plots into your brief write-up. Do not submit only a spreadsheet with
timings.