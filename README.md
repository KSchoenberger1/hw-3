# HW 3

Convert the serial (or your OpenMP)n-body problem code into a distributed-memory parallel code using the Message Passing Interface
(MPI). 

Run a series of benchmarks on Stampede2 to measure the parallel speed-up and efficiency.