#!/bin/bash
#echo "begin testing"
#make clean
#make

#echo "2 copies running"
#ibrun -n 2 ./mpi_nbody3

#echo "need to determine the rank and total number of MPI processes at runtime"

#for i in '1 2 4 8 16 32 64 128'; do ibrun -n $i ./mpi_nbody3; done

for i in 1000 2000 4000 8000
    do
	echo "1 process $i particles"
	ibrun -n 1 ./mpi_nbody3 -n $i
	echo "2 process $i particles"
	ibrun -n 2 ./mpi_nbody3 -n $i
	echo "4 process $i particles"
	ibrun -n 4 ./mpi_nbody3 -n $i
	echo "8 process $i particles"
	ibrun -n 8 ./mpi_nbody3 -n $i
	echo "16 process $i particles"
	ibrun -n 16 ./mpi_nbody3 -n $i
	echo "32 process $i particles"
	ibrun -n 32 ./mpi_nbody3 -n $i
	echo "64 process $i particles"
	ibrun -n 64 ./mpi_nbody3 -n $i
	
        #TODO doesn't run
        echo "128 process $i particles"
	ibrun -n 128 ./mpi_nbody3 -n 1000
    done    

for i in 16000
    do
	echo "2 process $i particles"
	ibrun -n 2 ./mpi_nbody3 -n $i
	echo "4 process $i particles"
	ibrun -n 4 ./mpi_nbody3 -n $i
	echo "8 process $i particles"
	ibrun -n 8 ./mpi_nbody3 -n $i
	echo "16 process $i particles"
	ibrun -n 16 ./mpi_nbody3 -n $i
	echo "32 process $i particles"
	ibrun -n 32 ./mpi_nbody3 -n $i
	echo "64 process $i particles"
	ibrun -n 64 ./mpi_nbody3 -n $i
	
        #TODO doesn't run
        echo "128 process $i particles"
	ibrun -n 128 ./mpi_nbody3 -n 1000
    done    
    
#echo "run serial and parallel cases with 1k 2k 4k 8k and 16k particles using 1 2 4 8 16 32 64 128 processes on 4 nodes. do not run 16k case with 1 rank it takes too long"